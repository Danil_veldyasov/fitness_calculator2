package ru.startandroid.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity  {

    long a,b,c,d;

    EditText editText1;
    Button button2;
    EditText editText2;

    EditText editText3;
    CheckBox checkBox1;
    CheckBox checkBox2;
    CheckBox checkBox3;
    CheckBox checkBox4;
    CheckBox checkBox5;
    CheckBox checkBox6;
    CheckBox checkBox7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);


        editText1 = (EditText) findViewById(R.id.editText1);
        editText2 = (EditText) findViewById(R.id.editText2);
        editText3 = (EditText) findViewById(R.id.editText3);

        checkBox1 = (CheckBox) findViewById(R.id.checkbox1);
        checkBox2 = (CheckBox) findViewById(R.id.checkbox2);
        checkBox3 = (CheckBox) findViewById(R.id.checkbox3);
        checkBox4 = (CheckBox) findViewById(R.id.checkbox4);
        checkBox5 = (CheckBox) findViewById(R.id.checkbox5);
        checkBox6 = (CheckBox) findViewById(R.id.checkbox6);
        checkBox7 = (CheckBox) findViewById(R.id.checkbox7);

        button2 = (Button) findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                a=Long.parseLong(editText1.getText().toString());


                b = Long.parseLong(editText2.getText().toString());
                c = Long.parseLong(editText3.getText().toString());

                d =(long)  ((a * 9.99F) + (b * 6.25F) - (c * 4.92F));

                if (checkBox1.isChecked()) {
                    d = d + 5;
                    if (checkBox3.isChecked()) d = (long) (d * 1.2F);
                    if (checkBox4.isChecked()) d = (long) (d * 1.4F);
                    if (checkBox5.isChecked()) d = (long) (d * 1.6F);
                    if (checkBox6.isChecked()) d = (long) (d * 1.8F);
                    if (checkBox7.isChecked()) d = (long) (d * 2.0F);
                }
                if (checkBox2.isChecked()){
                    d = d - 161;
                    if (checkBox3.isChecked()) d = (long) (d * 1.2F);
                    if (checkBox4.isChecked()) d = (long) (d * 1.3F);
                    if (checkBox5.isChecked()) d = (long) (d * 1.5F);
                    if (checkBox6.isChecked()) d = (long) (d * 1.7F);
                    if (checkBox7.isChecked()) d = (long) (d * 1.9F);
                }


                String S = Long.toString(d);
                Intent intent = new Intent(MainActivity2.this , MainActivity3.class);
                Intent putExtra = intent.putExtra("remelt", S);
                startActivity(intent);

            }
        });

    }



}



























